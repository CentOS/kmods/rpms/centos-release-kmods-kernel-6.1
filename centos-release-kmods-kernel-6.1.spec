Name:             centos-release-kmods-kernel-6.1
Version:          %{?rhel}
Release:          3%{?dist}
Summary:          CentOS Kmods SIG package repositories for Kernel

License:          GPLv2
URL:              https://sigs.centos.org/kmods

Source0:          RPM-GPG-KEY-CentOS-SIG-Kmods
Source1:          centos-kmods-kernel-6.1.repo

BuildArch:        noarch


%description
This package provides the package repository files for Kernel by CentOS Kmods SIG.


%prep


%build


%install
%{__install} -m 644 -D -t %{buildroot}%{_sysconfdir}/pki/rpm-gpg %{SOURCE0}
%{__install} -m 644 -D -t %{buildroot}%{_sysconfdir}/yum.repos.d %{SOURCE1}

%if 0%{?rhel} < 10
find %{buildroot}%{_sysconfdir}/yum.repos.d -type f -name \*.repo -exec sed -i 's/$releasever_major/%{?rhel}/g' \{\} \;
%endif


%clean
%{__rm} -rf %{buildroot}


%files
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-Kmods
%config(noreplace) %{_sysconfdir}/yum.repos.d/centos-kmods-kernel-6.1.repo


%changelog
* Fri Nov 22 2024 Peter Georg <peter.georg@physik.uni-regensburg.de> - %{?rhel}-3
- Replace $releasever by $releasever_major
- Replace $releasever_major with fixed value for rhel < 10

* Fri May 24 2024 Peter Georg <peter.georg@physik.uni-regensburg.de> - %{?rhel}-2
- el8 repositories have been moved to mirror.stream.centos.org
- Exclude kernel-headers and kernel-cross-headers

* Fri Mar 15 2024 Peter Georg <peter.georg@physik.uni-regensburg.de> - %{?rhel}-1
- Initial release
